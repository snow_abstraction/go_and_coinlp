// Copyright 2019 Douglas Wayne Potter. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

// #cgo LDFLAGS: -L/usr/lib/x86_64-linux-gnu/ -lClpSolver -lClp
// #include <stdio.h>
// #include <stdlib.h>
//
// #include <coin/Clp_C_Interface.h>
//
import "C"

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"unsafe"
)

func setupAbortSignalHandler() {
	// Running this function prevents pages of crash info from being printed.
	// Of course this is dangerous since it could mask other Abort Signals.
	//
	// The crash in mind is caused by an assert in C++ code. There is enough
	// output from the C++ code:
	//
	//     ClpSimplexNonlinear.cpp:2117: int ClpSimplexNonlinear::pivotColumn(CoinIndexedVector*, CoinIndexedVector*, CoinIndexe
	//     dVector*, CoinIndexedVector*, int&, double&, double*): Assertion `theta_ < 1.0e30' failed.
	//
	// and this seems to have been solved in https://github.com/coin-or/Clp/commit/dec929696.
	//
	// Strangely, the message of "Abort Signal caught." is never displayed.
	// I suspect the issues in section "Go programs that use cgo or SWIG" of
	// https://golang.org/pkg/os/signal/

	abortSignalChan := make(chan os.Signal, 1)
	signal.Notify(abortSignalChan, syscall.SIGABRT)
	go func() {
		<-abortSignalChan
		log.Fatal("\nAbort Signal caught.")
		os.Exit(1)
	}()
}

func run() error {
	setupAbortSignalHandler()

	if len(os.Args) < 2 {
		fmt.Println("Usage: msp_exp <msp file>")
		os.Exit(1)
	}

	model := C.Clp_newModel()
	defer func() { C.Clp_deleteModel(unsafe.Pointer(model)) }()

	modelPath := os.Args[1]
	cModelPath := C.CString(modelPath)
	defer func() { C.free(unsafe.Pointer(cModelPath)) }()

	if status := C.Clp_readMps(model, cModelPath, 1, 0); status != 0 {
		return fmt.Errorf("Failed to load model %s", modelPath)
	}

	C.Clp_initialDualSolve(model)

	return nil
}

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
}
